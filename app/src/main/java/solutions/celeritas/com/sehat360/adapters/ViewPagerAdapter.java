package solutions.celeritas.com.sehat360.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import solutions.celeritas.com.sehat360.Fragments.ageFragment;
import solutions.celeritas.com.sehat360.Fragments.genderFragment;
import solutions.celeritas.com.sehat360.Fragments.profileFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new genderFragment(); //ChildFragment1 at position 0
            case 1:
                return new ageFragment(); //ChildFragment2 at position 1
            case 2:
                return new profileFragment(); //ChildFragment3 at position 2
        }
        return null; //does not happen
    }

    @Override
    public int getCount() {
        return 3; //three fragments
    }
}