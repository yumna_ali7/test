package solutions.celeritas.com.sehat360.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import solutions.celeritas.com.sehat360.R;
import solutions.celeritas.com.sehat360.models.activationCode.activation_wrapper;
import solutions.celeritas.com.sehat360.networking.ApiController;
import solutions.celeritas.com.sehat360.utils.Constants;
import solutions.celeritas.com.sehat360.utils.UtilityClass;
import solutions.celeritas.com.sehat360.views.CustomTextViewLight;

public class activation extends AppCompatActivity {

    EditText activation_editText;
    Button btn_activate;
    String activation_code;
    CustomTextViewLight create_account;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activation);
        init();
    }
    public void init(){
        activation_editText = (EditText) findViewById(R.id.activation_editText);
        btn_activate = (Button) findViewById(R.id.btn_activate);
        create_account = (CustomTextViewLight) findViewById(R.id.create_account);
        if(getIntent().getStringExtra(Constants.USER_EMAIL)!= null){
            email = getIntent().getStringExtra(Constants.USER_EMAIL);
           // activation_editText.setText(getResources().getString(R.string.account)+" " +email);
        }
        setActivation_code();
        setCreate_account();
    }

    public void setActivation_code(){
        btn_activate.setOnClickListener(v -> {
            activation_code = activation_editText.getText().toString();
            if (activation_code.isEmpty()){
                UtilityClass.showPopup(activation.this,getResources().getString(R.string.error), getResources().getString(R.string.activation_code_required));
            }
            accountActivation(activation_code,email);
        });

    }

    public void setCreate_account(){create_account.setOnClickListener(v -> {
            startActivity(new Intent(this,register.class));
    });
    }


    public void accountActivation(final String activation_code , String email){
        ApiController.getInstance(this).getAPIService().Activation(activation_code,email).enqueue(new Callback<activation_wrapper>() {

            @Override
            public void onResponse(Call<activation_wrapper> call, Response<activation_wrapper> response) {
                try {
                    JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    activation_wrapper activation_wrapper =(activation_wrapper) new Gson().fromJson(jsonObject.toString(), activation_wrapper.class);
                    if (activation_wrapper.getResponse().getSuccess()){
                        Intent intent = new Intent(activation.this, chat.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Constants.USER_EMAIL,email);
                        startActivity(intent);
                        finish();
                        activation_editText.setText("");
                    }
                    else {
                        UtilityClass.showPopup(activation.this, getResources().getString(R.string.alert),
                                getResources().getString(R.string.incorrect_activation));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<activation_wrapper> call, Throwable t) {

            }
        });

    }


}
