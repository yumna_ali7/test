package solutions.celeritas.com.sehat360.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by KANAYA LAL & YUMNA ALI on 8/7/2018.
 */

public class CustomTextViewLight extends TextView {
    public CustomTextViewLight(Context context) {
        super(context);
        init();
    }

    public CustomTextViewLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }



    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "opensans_light.ttf");
        setTypeface(tf);
    }
}
