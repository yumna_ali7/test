package solutions.celeritas.com.sehat360.adapters;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.os.PowerManager;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.victor.loading.rotate.RotateLoading;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import nl.changer.audiowife.AudioWife;
import solutions.celeritas.com.sehat360.R;
import solutions.celeritas.com.sehat360.models.Messages;

import static com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread;

public class ChatList_Adapter extends RecyclerView.Adapter<ChatList_Adapter.ViewHolder> {
    private ArrayList<Messages> messagesArrayList;
    private Context mContext;
    RotateLoading rotateLoading;
    MediaPlayer mediaPlayer;
    boolean wasPlaying = false;
    private static SimpleDateFormat timeFormatter = new SimpleDateFormat("m:ss", Locale.getDefault());
    private Runnable mRunnable;
    private Handler mHandler;
    private int playTime;


    public ChatList_Adapter(ArrayList<Messages> messagesArrayList, Context mContext) {
        this.messagesArrayList = messagesArrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_item_view, parent, false);

        return new ViewHolder(itemView);
    }

    public static String getAudioTime(long time) {
        time *= 1000;
        timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return timeFormatter.format(new Date(time));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Messages messages = messagesArrayList.get(position);

        //For Voice Message
        if (messages.type == Messages.TYPE_AUDIO) {
            holder.recordtime_txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mic_small, 0, 0, 0);
            holder.recordtime_txt.setText(String.valueOf(getAudioTime(messages.audiotime)));
            Log.e("recordingtime", "" + messages.getmAudioTime());
            if (messages.getAudio() == null) {
                holder.audioLayout.setVisibility(View.GONE);
            } else {
                holder.audioLayout.setVisibility(View.VISIBLE);

                holder.play_ic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        try {
                            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                                mediaPlayer.stop();
                                mediaPlayer.release();
                                mediaPlayer = null;
                                //  holder.simpleSeekBar.setIndeterminate(false);

                                holder.simpleSeekBar.setProgress(0);
                                wasPlaying = true;
                                holder.play_ic.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.drawable.ic_media_play));
                            }

                            if (!wasPlaying) {
                                if (mediaPlayer == null) {
                                    mediaPlayer = new MediaPlayer();
                                }

                                holder.play_ic.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.drawable.ic_media_pause));


                                mediaPlayer.setDataSource(messages.getAudio());
                                Log.e("Filepath", "" + messages.getAudio());

                                mediaPlayer.prepare();
                                mediaPlayer.setVolume(0.5f, 0.5f);
                                mediaPlayer.setLooping(false);
                                mediaPlayer.start();
                                final Handler mHandler = new Handler();
//Make sure you update Seekbar on UI thread
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        if (mediaPlayer != null) {

                                            holder.simpleSeekBar.setProgress(mediaPlayer.getCurrentPosition());
                                            holder.simpleSeekBar.setMax(mediaPlayer.getDuration());
                                            mHandler.postDelayed(this, 1000);
                                        }


                                    }
                                });


                                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        holder.play_ic.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.drawable.ic_media_play));
                                        mediaPlayer.stop();
                                        mediaPlayer.release();
                                        mediaPlayer = null;
                                        holder.simpleSeekBar.setIndeterminate(false);
                                        holder.simpleSeekBar.setProgress(0);
                                    }
                                });

                            }

                            wasPlaying = false;


                        } catch (Exception e) {
                            e.printStackTrace();

                        }


                        Toast.makeText(mContext, "Recording Playing",
                                Toast.LENGTH_LONG).show();


                    }
                });
                // Set a change listener for seek bar
                holder.simpleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {


                        if (i > 0 && mediaPlayer != null && !mediaPlayer.isPlaying()) {
                            mediaPlayer.stop();
                            mediaPlayer.release();
                            mediaPlayer = null;

                            //holder.simpleSeekBar.setIndeterminate(false);
                            holder.play_ic.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.drawable.ic_media_play));
                            // seekBar.setProgress(0);
                            seekBar.setProgress(mediaPlayer.getDuration());
                            int _progress = (i * mediaPlayer.getDuration()) / 100;
                            mediaPlayer.seekTo(_progress); // PROBLEM SO
                            //   mediaPlayer.seekTo(i * 10);


                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                            mediaPlayer.seekTo(seekBar.getProgress());
                        }
                    }
                });
























                       /* try {
                            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                                mediaPlayer.reset();
                                clearMediaPlayer();

                                holder.simpleSeekBar.setProgress(0);
                                wasPlaying = true;
                                holder.play_ic.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.drawable.ic_media_play));
                            }

                            if (!wasPlaying) {
                                if (mediaPlayer == null) {
                                    mediaPlayer = new MediaPlayer();
                                }

                                holder.play_ic.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.drawable.ic_media_pause));
                                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                mediaPlayer.setDataSource(mContext, Uri.parse(messages.getAudio()));
                                mediaPlayer.prepare();
                                mediaPlayer.setVolume(0.5f, 0.5f);
                                mediaPlayer.setLooping(true);
                                mediaPlayer.start();
                                final Handler mHandler = new Handler();
//Make sure you update Seekbar on UI thread
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        if (mediaPlayer != null) {

                                            holder.simpleSeekBar.setProgress(mediaPlayer.getCurrentPosition());
                                            holder.simpleSeekBar.setMax(mediaPlayer.getDuration());
                                            mHandler.postDelayed(this, 1000);
                                        }


                                    }
                                });


                                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        holder.play_ic.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.drawable.ic_media_play));
                                        clearMediaPlayer();
                                        holder.simpleSeekBar.setProgress(0);
                                    }
                                });

                            }

                            wasPlaying = false;


                        } catch (Exception e) {
                            e.printStackTrace();

                        }


                        Toast.makeText(mContext, "Recording Playing",
                                Toast.LENGTH_LONG).show();
*/
/*
                        try {

                            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                                clearMediaPlayer();

                                holder.simpleSeekBar.setProgress(0);
                                wasPlaying = true;
                                holder.play_ic.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.drawable.ic_media_play));
                            }

                            if (!wasPlaying) {
                                if (mediaPlayer == null) {
                                    mediaPlayer = new MediaPlayer();
                               }

                                holder.play_ic.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.drawable.ic_media_pause));

                                mediaPlayer.setDataSource(messages.getAudio());
                                mediaPlayer.prepareAsync();
                                mediaPlayer.setVolume(0.5f, 0.5f);
                                mediaPlayer.setLooping(false);
                                mediaPlayer.start();
                         *//*       final Handler mHandler = new Handler();
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        if (mediaPlayer != null) {

                                            holder.simpleSeekBar.setProgress(mediaPlayer.getCurrentPosition());
                                            holder.simpleSeekBar.setMax(mediaPlayer.getDuration());
                                            mHandler.postDelayed(this, 1000);
                                        }
                                    }
                                });*//*
                 *//*  mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        holder.play_ic.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.drawable.ic_media_play));
                                        clearMediaPlayer();
                                        holder.simpleSeekBar.setProgress(0);
                                    }
                                });*//*

                            }

                            wasPlaying = false;

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                        Toast.makeText(mContext, "Recording Playing",
                                Toast.LENGTH_LONG).show();*/
            }

            // Set a change listener for seek bar
              /*  holder.simpleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {


                        if (i > 0 && mediaPlayer != null && !mediaPlayer.isPlaying()) {
                            clearMediaPlayer();
                            holder.play_ic.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.drawable.ic_media_play));
                            // seekBar.setProgress(0);
                            seekBar.setProgress(mediaPlayer.getDuration());
                            int _progress = (i * mediaPlayer.getDuration()) / 100;
                            mediaPlayer.seekTo(_progress); // PROBLEM SO
                            //   mediaPlayer.seekTo(i * 10);


                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                            mediaPlayer.seekTo(seekBar.getProgress());
                        }
                    }
                });*/

        } else if (messages.type == Messages.TYPE_TEXT) {
            holder.mText_msg.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            holder.mText_msg.setText(messages.getmText());

        } else {
            holder.mText_msg.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            holder.mText_msg.setText("");

        }


        // Images
        if (messages.getmImg() == null) {
            holder.mImage_msg.setVisibility(View.GONE);
            holder.img_layout.setVisibility(View.GONE);
            holder.mText_msg.setVisibility(View.VISIBLE);
            holder.text_layout.setVisibility(View.VISIBLE);
            holder.videoLayout.setVisibility(View.GONE);
            holder.docLayout.setVisibility(View.GONE);

        } else {
            holder.mImage_msg.setVisibility(View.VISIBLE);
            holder.img_layout.setVisibility(View.VISIBLE);
            holder.mText_msg.setVisibility(View.GONE);
            holder.text_layout.setVisibility(View.GONE);
            holder.videoLayout.setVisibility(View.GONE);
            holder.docLayout.setVisibility(View.GONE);
            holder.audioLayout.setVisibility(View.GONE);

            // holder.mImage_msg.setImageURI(Uri.parse(messages.getmImg()));

            Glide.with(mContext)
                    .load(Uri.parse(messages.getmImg()))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.homeprogress.setVisibility(View.GONE);
                            //   progressBar.setVisibility(View.GONE);
                            // rotateLoading.start();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            //   progressBar.setVisibility(View.GONE);
                            holder.homeprogress.setVisibility(View.GONE);
                            // rotateLoading.stop();

                            return false;
                        }
                    })
                    .into(holder.mImage_msg);
            holder.img_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog nagDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    nagDialog.setCancelable(true);
                    nagDialog.setContentView(R.layout.preview_img);
                    ImageView btnClose = (ImageView) nagDialog.findViewById(R.id.btnIvClose);
                    ImageView ivPreview = (ImageView) nagDialog.findViewById(R.id.iv_preview_image);
                    ivPreview.setImageURI(Uri.parse(messages.getmImg()));
                    //  ivPreview.setBackgroundDrawable(dd);

                    btnClose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View arg0) {

                            nagDialog.dismiss();
                        }
                    });
                    nagDialog.show();
                }
            });

            // holder.mImage_msg.setImageURI(messages.getmImg());
        }

        //Video
        if (messages.getmVideo() == null) {
            holder.videoLayout.setVisibility(View.GONE);

        } else {
            holder.videoLayout.setVisibility(View.VISIBLE);
            holder.text_layout.setVisibility(View.GONE);
            holder.docLayout.setVisibility(View.GONE);

            holder.video_msg.setVideoURI(Uri.parse(messages.getmVideo()));

            holder.videoLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog nagDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    nagDialog.setCancelable(true);
                    nagDialog.setContentView(R.layout.preview_video);
                    ImageView btnClose = (ImageView) nagDialog.findViewById(R.id.btnIvClose);
                    final ImageView playbtn_ic = (ImageView) nagDialog.findViewById(R.id.play_icon);
                    final ImageView pause_btn = (ImageView) nagDialog.findViewById(R.id.pause_icon);
                    final VideoView ivPreview = (VideoView) nagDialog.findViewById(R.id.videoview);
                    ivPreview.setVideoURI(Uri.parse(messages.getmVideo()));

                    ivPreview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(final MediaPlayer mp) {

                            mp.start();
                            mp.setLooping(false);
                            ivPreview.start();
                            playbtn_ic.setImageDrawable(ContextCompat.getDrawable(mContext,android.R.drawable.ic_media_pause));

                            playbtn_ic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (ivPreview.isPlaying()){
                                        playbtn_ic.setImageDrawable(ContextCompat.getDrawable(mContext,android.R.drawable.ic_media_play));
                                        mp.stop();

                                    }
                                    else {
                                        playbtn_ic.setImageDrawable(ContextCompat.getDrawable(mContext,android.R.drawable.ic_media_pause));
                                        ivPreview.resume();
                                        ivPreview.start();
                                    }
                                }
                            });

                        }

                    });
                    ivPreview.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            playbtn_ic.setImageDrawable(ContextCompat.getDrawable(mContext,android.R.drawable.ic_media_play));

                        }
                    });


                   /* playbtn_ic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {*/



                        /*    if (ivPreview.isPlaying() && mp.isPlaying()){
                                mp.stop();
                                play_btn.setImageDrawable(ContextCompat.getDrawable(mContext,android.R.drawable.ic_media_play));
                            }
                            else if (!mp.isPlaying() && !ivPreview.isPlaying()){
                                try {
                                    mp.prepare();
                                    ivPreview.start();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                mp.start();
                                mp.setLooping(false);
                                ivPreview.start();
                                play_btn.setImageDrawable(ContextCompat.getDrawable(mContext,android.R.drawable.ic_media_pause));
                            }*/

              /*          }
                    });*/



                    //  ivPreview.setBackgroundDrawable(dd);

                    btnClose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View arg0) {

                            nagDialog.dismiss();
                        }
                    });
                    nagDialog.show();
                }
            });

        }

        //Document
        if (messages.getmDocument() == null) {
            holder.docLayout.setVisibility(View.GONE);
        } else {
            holder.docLayout.setVisibility(View.VISIBLE);
            holder.text_layout.setVisibility(View.GONE);


            holder.doc_msg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File pdfFile = new File(messages.getmDocument());
                    Log.e("file", "" + pdfFile);
                    if (pdfFile.exists()) //Checking if the file exists or not
                    {
                        Uri pdfPath = FileProvider.getUriForFile(mContext, mContext.getApplicationContext().getPackageName() + ".fileprovider", pdfFile);
                        //  Uri pdfPath = Uri.fromFile(pdfFile);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(pdfPath, "application/pdf");
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                        try {
                            mContext.startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            //if user doesn't have pdf reader instructing to download a pdf reader
                        }

                    } else {

                        Toast.makeText(mContext.getApplicationContext(), "The file not exists! ", Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }
        if (messages.type == Messages.TYPE_AUDIO) {
            holder.text_layout.setVisibility(View.GONE);
        } else {
            holder.mText_msg.setText(messages.getmText());
            holder.mTime_text.setText(messages.getmTime());
            holder.audioLayout.setVisibility(View.GONE);
        }

        holder.Imgtime_text.setText(messages.getmTime());
        holder.videotime_text.setText(messages.getmTime());
        holder.time_text4.setText(messages.getmTime());
        holder.time_text5.setText(messages.getmTime());


    }


    @Override
    public int getItemCount() {
        return messagesArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mText_msg, mTime_text, Imgtime_text, videotime_text, time_text4, time_text5, recordtime_txt;
        ImageView mImage_msg, play_btn, doc_msg, play_ic;
        VideoView video_msg;
        ProgressBar homeprogress;
        SeekBar simpleSeekBar;
        RelativeLayout img_layout, videoLayout, docLayout, audioLayout;

        ConstraintLayout text_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mText_msg = (TextView) itemView.findViewById(R.id.text_msg);
            mImage_msg = (ImageView) itemView.findViewById(R.id.img_msg);
            doc_msg = (ImageView) itemView.findViewById(R.id.doc_msg);
            play_ic = (ImageView) itemView.findViewById(R.id.play_ic);
            play_btn = (ImageView) itemView.findViewById(R.id.play_btn);

            homeprogress = (ProgressBar) itemView.findViewById(R.id.progress_bar);
            simpleSeekBar = (SeekBar) itemView.findViewById(R.id.simpleSeekBar);
            video_msg = (VideoView) itemView.findViewById(R.id.video_msg);

            img_layout = (RelativeLayout) itemView.findViewById(R.id.img_layout);
            videoLayout = (RelativeLayout) itemView.findViewById(R.id.videoLayout);
            text_layout = (ConstraintLayout) itemView.findViewById(R.id.text_layout);
            docLayout = (RelativeLayout) itemView.findViewById(R.id.documentLayout);
            audioLayout = (RelativeLayout) itemView.findViewById(R.id.audio_layout);

            mTime_text = (TextView) itemView.findViewById(R.id.time_text);
            Imgtime_text = (TextView) itemView.findViewById(R.id.time_text2);
            videotime_text = (TextView) itemView.findViewById(R.id.time_text3);
            time_text4 = (TextView) itemView.findViewById(R.id.time_text4);
            time_text5 = (TextView) itemView.findViewById(R.id.time_text5);
            recordtime_txt = (TextView) itemView.findViewById(R.id.recordtime_txt);

// AudioWife takes care of click handler for play/pause button

        }


    }
}


