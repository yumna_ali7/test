package solutions.celeritas.com.sehat360.networking;

import android.content.Context;


import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import solutions.celeritas.com.sehat360.utils.Constants;

/**
 * Created by Developer on 3/12/2018.
 */
public class ApiController {
    private static ApiController instance;
    private static Context context;
    private WebService apiService;
    /**
     * No argument PRIVATE constructor
     */
    private ApiController() {}
    /**
     * Method for getting the only instance of this class
     *
     * @param context
     * @return
     */
    public static ApiController getInstance(Context context) {
        ApiController.context = context.getApplicationContext();
        if (instance == null) {
            instance = new ApiController();
        }
        return instance;
    }
    /**
     * This method is used for getting ServiceInterface object
     *
     * @return
     */
    public WebService getAPIService(){
        if(this.apiService == null) {
            setAPIService();
        }
        return this.apiService;
    }
    private final Interceptor CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            String cacheHeaderValue = Constants
                    .isNetworkAvailable(ApiController.context)
                    ? "public, max-age=2400"
                    : "public, only-if-cached, max-stale=2400";
            Request request = originalRequest.newBuilder().build();
            Response response = chain.proceed(request);
            return response.newBuilder()
                    .removeHeader("Pragma")
                    .removeHeader("Cache-Control")
                    .header("Cache-Control", cacheHeaderValue)
                    .build();
        }
    };
    /**
     * This method initializes the ServiceInterface object: 'apiService'
     */
    private void setAPIService() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        OkHttpClient okHttpClient = new OkHttpClient();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Initializing OkHttpClient
        okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(CACHE_CONTROL_INTERCEPTOR)
                .retryOnConnectionFailure(true)
                .addNetworkInterceptor(CACHE_CONTROL_INTERCEPTOR)
                .addInterceptor(loggingInterceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
        // Initializing apiService using anonymous RestAdapter
        this.apiService = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build()
                .create(WebService.class);
    }
}