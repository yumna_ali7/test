package solutions.celeritas.com.sehat360.utils;

import android.content.Context;


import solutions.celeritas.com.sehat360.models.signIn.UserModel;

public class SessionClass {

    private static SessionClass mInstance;
    private String userPassword;
    private String userEmail;
    private int userId;
    private String firstName;
    private boolean isLogin;

    private SessionClass() {
        //no instance
    }

    public static SessionClass getInstance() {
        if (mInstance == null)
            mInstance = new SessionClass();
        return mInstance;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstname(String firstname) {
        this.firstName = firstname;
    }

    public UserModel getUser(Context context) {
        return ((UserModel) new SharedPrefManager(context).
                getObjectFromSharedPref(Constants.PROFILE, UserModel.class));
    }

    public void saveUserInfo(Context context, UserModel user) {
        new SharedPrefManager(context).
                saveObjectInSharedPref(user, Constants.PROFILE);
    }

  /*  public PetModel getPetModel(Context context) {
        return ((PetModel) new SharedPrefManager(context).
                getObjectFromSharedPref(Constants.PET_MODEL_OBJECT, PetModel.class));
    }*/
/*
    public PetModelUpdated getPetModelUpdated(Context context) {
        return ((PetModelUpdated) new SharedPrefManager(context).
                getObjectFromSharedPref(Constants.PET_MODEL_OBJECT, PetModelUpdated.class));
    }
*/

//    public void save_updatePetModel(Context context, PetModel petModel) {
//        new SharedPrefManager(context).
//                saveObjectInSharedPref(petModel, Constants.PET_MODEL_OBJECT);
//    }
//    public void saveUpdatePetModelUpdated(Context context, PetModelUpdated petModel) {
//        new SharedPrefManager(context).
//                saveObjectInSharedPref(petModel, Constants.PET_MODEL_OBJECT);
//    }
//
//
//    public void saveCollectionTOSharedPref(Context context, Object collection, String key) {
//        String value = new Gson().toJson(collection);
//        new SharedPrefManager(context).setStringForKey(value, key);
//    }
//
//    public void clearSession(Context context) {
//        //new SharedPrefManager(context).clearPreferences();
//        new SharedPrefManager(context).saveObjectInSharedPref(null, Constants.PET_MODEL_OBJECT);
//        new SharedPrefManager(context).saveObjectInSharedPref(null, Constants.PROFILE);
//    }

}
