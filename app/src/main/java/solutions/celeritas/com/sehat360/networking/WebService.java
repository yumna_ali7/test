package solutions.celeritas.com.sehat360.networking;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import solutions.celeritas.com.sehat360.models.activationCode.activation_wrapper;
import solutions.celeritas.com.sehat360.models.signIn.signIn_wrapper;

public interface WebService {
    @FormUrlEncoded
    @POST("insert/content")
    Call<Object> InsertContent(
            @Field("user_id") String user_id,
            @Field("section_id") String section_id,
            @Field("content_type") String content_type,
            @Field("content_location") String content_location,
            @Field("text") String text
    );

    @Headers({"x-access-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQ2hyaXMiLCJ1c2VybmFtZSI6ImNocmlzIiwiaWF0IjoxNTUxMTc3Nzg5LCJleHAiOjE1NTEyNjQxODl9.xNXzXXVahJKrUjA2eZJib-yj1_O1kmDd9h-BK0OfOgA"})
    @FormUrlEncoded
    @POST("register")
    Call<Object> register(@Field("name") String name ,
                                     @Field("emailOrNumber") String emailOrNumber,
                                     @Field("password") String password,
                                     @Field("confirm_password") String confirm_password  , @Header("Authorization") String authHeader);


    @FormUrlEncoded
    @POST("login")
    Call<signIn_wrapper> login(@Field("emailOrNumber") String email,
                               @Field("password") String password);

    @FormUrlEncoded
    @POST("account/activate")
    Call<activation_wrapper> Activation(@Field("activation_code") String activation_code,
                                        @Field("email") String email);

}