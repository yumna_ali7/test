package solutions.celeritas.com.sehat360.models.signIn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class signIn_wrapper {

    @SerializedName("response")
    @Expose
    private signIn_response response;

    public signIn_response getResponse() {
        return response;
    }

    public void setResponse(signIn_response response) {
        this.response = response;
    }

}
