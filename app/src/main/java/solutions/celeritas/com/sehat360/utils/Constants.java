package solutions.celeritas.com.sehat360.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Constants {
    /**
     *  BasicAWSCredentials
     */
    public static String KEY = "AKIAJ4DDXAATIVJBCJPQ";
    public static String SECRET = "Y5BiZLhT7uX74vHI+My/D6ZJ3mbZvtHYkD9+56jr";
    public static String BUCKET_NAME= "jamshed";

    public static String NOT_CONNECT_WITH_INTERNET = "Please Connect Your Device With Internet";
    public static String NETWORK_NOT_FOUND = "Check your connection and try again";
    public static String BASE_URL = "http://ec2-3-92-174-152.compute-1.amazonaws.com:8383/api/"
          ;
    public static String IMG_CONTENT_TYPE= "1";
    public static String Video_CONTENT_TYPE= "2";
    public static String Audio_CONTENT_TYPE= "3";
    public static String Text_CONTENT_TYPE= "4";
    public static String PDF_CONTENT_TYPE= "5";

    public static String Authorization_Header= "Bearer " + "EC-7K123899D1969560R";
    public static String X_APP_TOKEN = "x-access-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQ2hyaXMiLCJ1c2VybmFtZSI6ImNocmlzIiwiaWF0IjoxNTUxMTc3Nzg5LCJleHAiOjE1NTEyNjQxODl9.xNXzXXVahJKrUjA2eZJib-yj1_O1kmDd9h-BK0OfOgA";


    public static String USER_NAME = "UserName";
    public static String USER_EMAIL = "UserEmail";
    public static String USER_PASSWORD = "UserPassword";
    public static String USER_ID = "UserID";
    public static String ACTIVATION_CODE = "ActivationCode";


    public static String PROFILE = "profileFragment";

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void callNetworkAlertDialoge(Context context) {
        showOkAlert(context, NETWORK_NOT_FOUND, NOT_CONNECT_WITH_INTERNET);
    }

    public static void showOkAlert(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .show();

    }


}