package solutions.celeritas.com.sehat360.adapters;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import solutions.celeritas.com.sehat360.R;
import solutions.celeritas.com.sehat360.models.Message;

import static com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    private List<Message> messages = new ArrayList<>();
    MediaPlayer mediaPlayer;
    boolean wasPlaying = false;
    ;
    private static SimpleDateFormat timeFormatter = new SimpleDateFormat("m:ss", Locale.getDefault());
    private Runnable mRunnable;
    private Handler mHandler;

    public void add(Message message) {
        messages.add(message);
        notifyItemInserted(messages.lastIndexOf(message));
    }


    public MessageAdapter(Context context, ArrayList<Message> messages) {
        this.context = context;
        this.messages = messages;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_message, null);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MessageViewHolder) holder).bind(messages.get(position));
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static String getAudioTime(long time) {
        time *= 1000;
        timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return timeFormatter.format(new Date(time));
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        public TextView text;
        SeekBar simpleSeekBar;
        ImageView play_ic;
        RelativeLayout audio_layout;

        public MessageViewHolder(View view) {
            super(view);
            text = itemView.findViewById(R.id.textView);
            audio_layout = itemView.findViewById(R.id.audio_layout);
            simpleSeekBar = itemView.findViewById(R.id.simpleSeekBar);
            play_ic = itemView.findViewById(R.id.play_ic);
        }


        public void bind(final Message message) {

            if (message.type == Message.TYPE_AUDIO) {
                text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mic_small, 0, 0, 0);
                text.setText(String.valueOf(getAudioTime(message.time)));

                play_ic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                                clearMediaPlayer();

                                simpleSeekBar.setProgress(0);
                                wasPlaying = true;
                                play_ic.setImageDrawable(ContextCompat.getDrawable(context, android.R.drawable.ic_media_play));
                            }

                            if (!wasPlaying) {
                                if (mediaPlayer == null) {
                                    mediaPlayer = new MediaPlayer();
                                }

                                play_ic.setImageDrawable(ContextCompat.getDrawable(context, android.R.drawable.ic_media_pause));


                                mediaPlayer.setDataSource(message.getAudio());

                                mediaPlayer.prepare();
                                mediaPlayer.setVolume(0.5f, 0.5f);
                                mediaPlayer.setLooping(false);
                                mediaPlayer.start();
                                final Handler mHandler = new Handler();
//Make sure you update Seekbar on UI thread
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        if (mediaPlayer != null) {

                                            simpleSeekBar.setProgress(mediaPlayer.getCurrentPosition());
                                            simpleSeekBar.setMax(mediaPlayer.getDuration());
                                            mHandler.postDelayed(this, 1000);
                                        }


                                    }
                                });


                                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        play_ic.setImageDrawable(ContextCompat.getDrawable(context, android.R.drawable.ic_media_play));
                                        clearMediaPlayer();
                                        simpleSeekBar.setProgress(0);
                                    }
                                });

                            }

                            wasPlaying = false;


                        } catch (Exception e) {
                            e.printStackTrace();

                        }


                        Toast.makeText(context, "Recording Playing",
                                Toast.LENGTH_LONG).show();


                    }
                });
                // Set a change listener for seek bar
                simpleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {


                        if (i > 0 && mediaPlayer != null && !mediaPlayer.isPlaying()) {
                            clearMediaPlayer();
                            play_ic.setImageDrawable(ContextCompat.getDrawable(context, android.R.drawable.ic_media_play));
                            // seekBar.setProgress(0);
                            seekBar.setProgress(mediaPlayer.getDuration());
                            int _progress = (i * mediaPlayer.getDuration()) / 100;
                            mediaPlayer.seekTo(_progress); // PROBLEM SO
                            //   mediaPlayer.seekTo(i * 10);


                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                            mediaPlayer.seekTo(seekBar.getProgress());
                        }
                    }
                });

                  /*    final Handler mHandler = new Handler();
                         //Make sure you update Seekbar on UI thread
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                if (mediaPlayer != null) {
                                    int mCurrentPosition = mediaPlayer.getCurrentPosition() / 1000;
                                    simpleSeekBar.setProgress(mCurrentPosition);
                                }
                                mHandler.postDelayed(this, 1000);
                            }
                        });*/

            } else if (message.type == Message.TYPE_TEXT) {
                text.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                text.setText(message.text);

            } else {
                text.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                text.setText("");
            }

        }

        public void run() {

            int currentPosition = mediaPlayer.getCurrentPosition();
            int total = mediaPlayer.getDuration();


            while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
                try {
                    Thread.sleep(1000);
                    currentPosition = mediaPlayer.getCurrentPosition();
                } catch (InterruptedException e) {
                    return;
                } catch (Exception e) {
                    return;
                }

                simpleSeekBar.setProgress(currentPosition);

            }
        }

        private void clearMediaPlayer() {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            simpleSeekBar.setIndeterminate(false);
        }
    }
}