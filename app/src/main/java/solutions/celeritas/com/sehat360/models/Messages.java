package solutions.celeritas.com.sehat360.models;



import android.widget.SeekBar;

import com.orm.SugarRecord;



public class Messages extends SugarRecord {
    String mText;
    String mImg;
    String mTime;
    String mVideo;
    String mDocument;
    public static int TYPE_TEXT = 1;
    public static int TYPE_AUDIO = 2;
    public int type;
    public int audiotime;
    String maudio;
    int mAudioTime;


    public Messages(String mText, String mImg, String time, String video,String document,String audio ,int audiotime) {
        this.mText = mText;
        this.mImg = mImg;
        this.mTime = time;
        this.mVideo =video;
        this.mDocument = document;
        this.maudio = audio;
        this.mAudioTime = audiotime;
        this.type = TYPE_TEXT;

    }

    public Messages() {
    }

    public int getmAudioTime() {
        return mAudioTime;
    }

    public void setmAudioTime(int mAudioTime) {
        this.mAudioTime = mAudioTime;
    }

    public Messages(int audiotime, String audio , String date) {
        this.audiotime = audiotime;
        this.maudio = audio;
        this.type = TYPE_AUDIO;
        this.mTime = date;
    }

    public String getAudio() {
        return maudio;
    }

    public void setAudio(String audio) {
        this.maudio = audio;
    }

    public String getmText() {
        return mText;
    }

    public void setmText(String mText) {
        this.mText = mText;
    }

    public String getmImg() {
        return mImg;
    }

    public String getmDocument() {
        return mDocument;
    }

    public void setmDocument(String mDocument) {
        this.mDocument = mDocument;
    }

    public void setmImg(String mImg) {
        this.mImg = mImg;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public String getmVideo() {
        return mVideo;
    }

    public void setmVideo(String mVideo) {
        this.mVideo = mVideo;
    }
}
