package solutions.celeritas.com.sehat360.models.activationCode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class activation_wrapper {

    @SerializedName("response")
    @Expose
    private activation_response response;

    public activation_response getResponse() {
        return response;
    }

    public void setResponse(activation_response response) {
        this.response = response;
    }

}
