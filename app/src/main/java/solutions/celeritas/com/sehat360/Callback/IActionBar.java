package solutions.celeritas.com.sehat360.Callback;

public interface IActionBar {
    void MenuClick();
    void LeftBackClick();
    void RightClick();
}
