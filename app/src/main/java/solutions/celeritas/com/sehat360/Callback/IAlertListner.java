package solutions.celeritas.com.sehat360.Callback;

public interface IAlertListner {
    void onYesClick();
    void onNoClick();
}
