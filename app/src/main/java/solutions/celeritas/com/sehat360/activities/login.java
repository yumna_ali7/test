package solutions.celeritas.com.sehat360.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import solutions.celeritas.com.sehat360.R;
import solutions.celeritas.com.sehat360.models.register.register_response;
import solutions.celeritas.com.sehat360.models.signIn.UserModel;
import solutions.celeritas.com.sehat360.models.signIn.signIn_response;
import solutions.celeritas.com.sehat360.models.signIn.signIn_wrapper;
import solutions.celeritas.com.sehat360.networking.ApiController;
import solutions.celeritas.com.sehat360.utils.Constants;
import solutions.celeritas.com.sehat360.utils.SessionClass;
import solutions.celeritas.com.sehat360.utils.SharedPrefManager;
import solutions.celeritas.com.sehat360.utils.UtilityClass;
import solutions.celeritas.com.sehat360.views.CustomTextViewLight;
import solutions.celeritas.com.sehat360.views.CustomTextViewRegular;

public class login extends AppCompatActivity {

    EditText mEmail;
    EditText mPassword;
    CheckBox remember_check;
    CustomTextViewRegular mForgotPassword;
    Button SignIn_btn;
    CustomTextViewLight Create_account;
    String userEmail, userPassword;
    SharedPrefManager sharedPrefManager;
    ImageView showhidepass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        init();
    }

    public void init() {
        mEmail = (EditText) findViewById(R.id.email_editText);
        mPassword = (EditText) findViewById(R.id.password_edtxt);
        remember_check = (CheckBox) findViewById(R.id.checkbox);
        mForgotPassword = (CustomTextViewRegular) findViewById(R.id.forgotpassword);
        Create_account = (CustomTextViewLight) findViewById(R.id.create_account);
        SignIn_btn = (Button) findViewById(R.id.btn_signIn);
        showhidepass = (ImageView) findViewById(R.id.showhidepass);


        setSignIn_btn();
        setCreate_account();
        mPassword.setTag(R.drawable.password_hide);
        sharedPrefManager = new SharedPrefManager(this);
        if (getIntent().getStringExtra(Constants.USER_EMAIL) != null) {
            mEmail.setText(getIntent().getStringExtra(Constants.USER_EMAIL));
        } else {
            if (new SharedPrefManager(this).getStringByKey(Constants.USER_EMAIL) != null && !new SharedPrefManager(this).getStringByKey(Constants.USER_EMAIL).isEmpty()) {
                mEmail.setText(new SharedPrefManager(this).getStringByKey(Constants.USER_EMAIL));
                mPassword.setText(new SharedPrefManager(this).getStringByKey(Constants.USER_PASSWORD));
                remember_check.setChecked(true);
            } else {
                remember_check.setChecked(false);
            }
        }
        showhidepassClick();
    }

    //Email validation
    public final static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    /**
     * Setting listener
     */
    public void setSignIn_btn() {
        SignIn_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userEmail = mEmail.getText().toString();
                userPassword = mPassword.getText().toString();
                sendUserDataToServer(userEmail, userPassword);
                if (userEmail.isEmpty() && userPassword.isEmpty()) {
                    UtilityClass.showPopup(login.this, getResources().getString(R.string.alert),
                            getResources().getString(R.string.all_field_must_be_enter));

                } else if (isValidEmail(userEmail)) {


                } else {
                    mEmail.setError("Invalid Email Address");
                    mEmail.requestFocus();
                }
            }
        });
    }

    public void setCreate_account() {
        Create_account.setOnClickListener(v -> {
            Intent intent = new Intent(login.this, register.class);
            startActivity(intent);
        });
    }

    public void sendUserDataToServer(String userEmail, String userPassword) {

        ApiController.getInstance(this).getAPIService().login(userEmail, userPassword).enqueue(new Callback<signIn_wrapper>() {

            @Override
            public void onResponse(Call<signIn_wrapper> call, Response<signIn_wrapper> response) {
           /*     if (response.isSuccessful())
                try {
                    JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    signIn_wrapper responseWrapper = (signIn_wrapper) new Gson().fromJson(jsonObject.toString(), signIn_wrapper.class);
                    Log.e("",""+responseWrapper.getResponse());
                }
                catch (Exception e){
                    e.printStackTrace();
                }*/
            /*    try {
                    signIn_response signIn_response= new signIn_response();
                    if (response.isSuccessful()) {

                        JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                        JSONObject jsonObject1 = (JSONObject) jsonObject.get("response");

                         if (jsonObject1.getBoolean("success")){

                             signIn_response.setMessage(jsonObject1.getString("message"));
                             signIn_response.setSuccess(jsonObject1.getBoolean("success"));
                            Log.e("","" +jsonObject1.getString("result"));
                            jsonObject1.getString("id");

                         }
                         else {
                             UtilityClass.showPopup(login.this, getResources().getString(R.string.alert),
                                     getResources().getString(R.string.incorrect));
                         }
                    }

                }  catch (JSONException e) {
                    e.printStackTrace();
                }*/


              JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    signIn_wrapper responseWrapper = (signIn_wrapper) new Gson().fromJson(jsonObject.toString(), signIn_wrapper.class);
                    responseWrapper.setResponse(response.body().getResponse());

                    if (responseWrapper.getResponse().getSuccess()) {

                        if (responseWrapper.getResponse().getUserModel().get(0).getIsActivated() == 1) {
                            if (remember_check.isChecked()) {
                                sharedPrefManager.setStringForKey(userEmail, Constants.USER_EMAIL);
                                sharedPrefManager.setStringForKey(userPassword, Constants.USER_PASSWORD);
                            } else {
                                sharedPrefManager.setStringForKey("", Constants.USER_EMAIL);
                                sharedPrefManager.setStringForKey("", Constants.USER_PASSWORD);
                            }
                            SessionClass.getInstance().saveUserInfo(login.this, responseWrapper.getResponse().getUserModel().get(0));

                            Intent intent = new Intent(login.this, chat.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();


                        } else {
                            UtilityClass.showPopup(login.this, getResources().getString(R.string.alert),
                                    getResources().getString(R.string.activate_your_account), () -> {
                                        Intent intent = new Intent(login.this, activation.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra(Constants.USER_EMAIL, userEmail);
                                        startActivity(intent);
                                        finish();
                                        mEmail.setText("");
                                        mPassword.setText("");
                                    });


                        }
                    }
                    else if (!responseWrapper.getResponse().getSuccess()){
                        UtilityClass.showPopup(login.this, getResources().getString(R.string.alert),
                                getResources().getString(R.string.incorrect));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
           }



            @Override
            public void onFailure(Call<signIn_wrapper> call, Throwable t) {
                UtilityClass.showPopup(login.this, getResources().getString(R.string.alert),
                        getResources().getString(R.string.internet ));
            }
        });

    }

    public void showhidepassClick() {

        showhidepass.setOnClickListener(v -> {
            Integer integer = (Integer) mPassword.getTag();
            switch (integer) {
                case R.drawable.password_hide: {
                    mPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                    ((ImageView) findViewById(R.id.showhidepass)).
                            setImageResource(R.drawable.password_show);
                    mPassword.setSelection(mPassword.getText().length());
                    Log.d("drawableclick", "true");

                    mPassword.setTag(R.drawable.password_show);
                    break;
                }

                case R.drawable.password_show: {
                    mPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    Log.d("drawableclick", "true");
                    mPassword.setSelection(mPassword.getText().length());
                    ((ImageView) findViewById(R.id.showhidepass)).
                            setImageResource(R.drawable.password_hide);
                    mPassword.setTag(R.drawable.password_hide);
                    break;
                }
            }


        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}




