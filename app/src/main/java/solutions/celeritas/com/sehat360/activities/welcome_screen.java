package solutions.celeritas.com.sehat360.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import solutions.celeritas.com.sehat360.R;

public class welcome_screen extends AppCompatActivity {


    Button register_btn, signIn_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);
        init();
    }

    public void init() {
        register_btn = (Button) findViewById(R.id.register_btn);
        signIn_btn = (Button) findViewById(R.id.signin_btn);

        setRegister_btn();
        setSignIn_btn();
    }


    /**
     * Setting listeners
     */

    public void setRegister_btn(){
        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(welcome_screen.this,register.class);
                startActivity(intent);
            }
        });
    }

    public void setSignIn_btn(){
        signIn_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent intent = new Intent(welcome_screen.this, login.class);
                 startActivity(intent);
            }
        });
    }
}
