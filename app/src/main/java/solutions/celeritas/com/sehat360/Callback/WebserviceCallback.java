package solutions.celeritas.com.sehat360.Callback;

public interface WebserviceCallback {
    void onResponseReceived(String jsonResponse);
    void onResponseNotReceived();
}
