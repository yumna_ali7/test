package solutions.celeritas.com.sehat360.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import solutions.celeritas.com.sehat360.R;


public class SamplePagerAdapter extends PagerAdapter {



   int[] arrayList ;



    public SamplePagerAdapter(int[] arrayList) {
        this.arrayList = arrayList;
    }

    @Override public int getCount() {
        return 3;
    }

    @Override public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup view, int position, @NonNull Object object) {
        view.removeView((View) object);
    }

    @NonNull @Override public Object instantiateItem(@NonNull ViewGroup view, int position) {
      View itemView = LayoutInflater.from(view.getContext()).inflate(arrayList[position],view,false);

      switch (arrayList[position]){
          case 0:
            RelativeLayout relativeLayout = itemView.findViewById(R.id.gender_img_layout);
            relativeLayout.setOnClickListener(v -> Log.e("gender",""));
              break;
          case 1:
              Log.e("Age","");
              break;
          case 2:
              Log.e("profile","");
              break;
      }

      view.addView(itemView);
      return itemView;
    }


}