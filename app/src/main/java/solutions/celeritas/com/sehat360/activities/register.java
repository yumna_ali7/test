package solutions.celeritas.com.sehat360.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import solutions.celeritas.com.sehat360.R;
import solutions.celeritas.com.sehat360.models.register.register_response;
import solutions.celeritas.com.sehat360.networking.ApiController;
import solutions.celeritas.com.sehat360.utils.Constants;
import solutions.celeritas.com.sehat360.utils.UtilityClass;
import solutions.celeritas.com.sehat360.views.CustomTextViewRegular;

public class register extends AppCompatActivity {


    EditText name_editText;
    EditText email_editText;
    EditText password_edtxt;
    EditText confirm_password_edtxt;
    Button signUp_btn;
    CustomTextViewRegular create_acc_tv;
ImageView showhide_confirmpass,showhidepass;
    String userName, userEmail, userPassword, userConfirmPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();

    }

    //Initialize
    public void init() {
        name_editText = (EditText) findViewById(R.id.name_editText);
        email_editText = (EditText) findViewById(R.id.email_editText);
        password_edtxt = (EditText) findViewById(R.id.password_edtxt);
        confirm_password_edtxt = (EditText) findViewById(R.id.confirm_password_edtxt);
        signUp_btn = (Button) findViewById(R.id.btn_signUp);
        create_acc_tv = (CustomTextViewRegular) findViewById(R.id.create_account);
        showhidepass = (ImageView) findViewById(R.id.showhidepass);
        showhide_confirmpass = (ImageView) findViewById(R.id.showhide_confirmpass) ;

        password_edtxt.setTag(R.drawable.password_hide);
        confirm_password_edtxt.setTag(R.drawable.password_show);
        setSignUp_btn();
        showhidepassClick();
        showhidepassconfirmClick();
        create_acc_tv.setOnClickListener(view -> {
            startActivity(new Intent(this,login.class));
            /*UtilityClass.showPopup(this, getString(R.string.terms_and_conditions),
                    getString(R.string.dummy_text));*/
        });
    }


    public void setSignUp_btn() {
        signUp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userName = name_editText.getText().toString();
                userEmail = email_editText.getText().toString();
                userPassword = password_edtxt.getText().toString();
                userConfirmPass = confirm_password_edtxt.getText().toString();
                if (userEmail.isEmpty() && userPassword.isEmpty() && userConfirmPass.isEmpty() &&userName.isEmpty()) {
                    UtilityClass.showPopup(register.this, getResources().getString(R.string.alert),
                            getResources().getString(R.string.all_field_must_be_enter));
                } else if (isValidEmail(userEmail) ) {
                    SignUp(userName, userEmail, userPassword, userConfirmPass);
                } else {
                    email_editText.setError("Invalid Email Address");
                    email_editText.requestFocus();
                }

            }
        });


    }

    //Email validation
    public final static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    public void SignUp(String name, String email, String password, String confirmPassword) {
        ApiController.getInstance(this).getAPIService().register(name, email, password, confirmPassword, Constants.Authorization_Header).enqueue(new Callback<Object>() {

            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if (response.isSuccessful()){
                    try {
                        JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                        JSONObject jsonObject1 = (JSONObject) jsonObject.get("response");
                        register_response register_response = new register_response();
                        register_response.setMessage(jsonObject1.getString("message"));
                        register_response.setSuccess(jsonObject1.getBoolean("success"));

                        if (register_response.getSuccess()) {
                            UtilityClass.showPopup(register.this, getResources().getString(R.string.success),
                                    getResources().getString(R.string.account_created), () -> {

                                        Intent intent = new Intent(register.this, login.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra(Constants.USER_EMAIL, email_editText.getText().toString());
                                        startActivity(intent);
                                        finish();
                                    });


                        } else {
                            UtilityClass.showPopup(register.this, getResources().getString(R.string.alert),
                                    getResources().getString(R.string.incorrect));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });
    }

    public void showhidepassClick() {
showhidepass.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Integer integer = (Integer) password_edtxt.getTag();
        switch (integer) {
            case R.drawable.password_hide: {
                password_edtxt.setInputType(InputType.TYPE_CLASS_TEXT);
                ((ImageView)findViewById(R.id.showhidepass)).
                        setImageResource(R.drawable.password_show);
                password_edtxt.setSelection(password_edtxt.getText().length());
                Log.d("drawableclick", "true");

                password_edtxt.setTag(R.drawable.password_show);
                break;
            }

            case R.drawable.password_show: {
                password_edtxt.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD);
                Log.d("drawableclick", "true");
                password_edtxt.setSelection(password_edtxt.getText().length());
                ((ImageView)findViewById(R.id.showhidepass)).
                        setImageResource(R.drawable.password_hide);
                password_edtxt.setTag(R.drawable.password_hide);
                break;
            }

        }
    }
});

    }

    public void showhidepassconfirmClick() {
        showhide_confirmpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer integer = (Integer) confirm_password_edtxt.getTag();
                switch (integer) {
                    case R.drawable.password_hide: {
                        confirm_password_edtxt.setInputType(InputType.TYPE_CLASS_TEXT);
                        ((ImageView)findViewById(R.id.showhide_confirmpass)).
                                setImageResource(R.drawable.password_show);
                        confirm_password_edtxt.setSelection(confirm_password_edtxt.getText().length());
                        Log.d("drawableclick", "true");

                        confirm_password_edtxt.setTag(R.drawable.password_show);
                        break;
                    }

                    case R.drawable.password_show: {
                        confirm_password_edtxt.setInputType(InputType.TYPE_CLASS_TEXT |
                                InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        Log.d("drawableclick", "true");
                        confirm_password_edtxt.setSelection(confirm_password_edtxt.getText().length());
                        ((ImageView)findViewById(R.id.showhide_confirmpass)).
                                setImageResource(R.drawable.password_hide);
                        confirm_password_edtxt.setTag(R.drawable.password_hide);
                        break;
                    }

                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
