package solutions.celeritas.com.sehat360.models.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class register_wrapper {

    @SerializedName("response")
    @Expose
    private register_response response;

    public register_response getResponse() {
        return response;
    }

    public void setResponse(register_response response) {
        this.response = response;
    }
}
