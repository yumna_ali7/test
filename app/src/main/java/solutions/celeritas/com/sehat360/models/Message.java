package solutions.celeritas.com.sehat360.models;

public class Message {

    public static int TYPE_TEXT = 1;
    public static int TYPE_AUDIO = 21;


    public String text;
    public int type;
    public int time;
    String audio;

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public Message(String text) {
        this.text = text;
        this.type = TYPE_TEXT;
    }

    public Message(int time,String audio) {
        this.time = time;
        this.audio = audio;
        this.type = TYPE_AUDIO;
    }

}