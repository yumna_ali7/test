package solutions.celeritas.com.sehat360.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;
import solutions.celeritas.com.sehat360.R;
import solutions.celeritas.com.sehat360.adapters.SamplePagerAdapter;
import solutions.celeritas.com.sehat360.adapters.ViewPagerAdapter;
import solutions.celeritas.com.sehat360.models.Viewpager_layout;

public class profile extends AppCompatActivity {

Button next_btn;
    ViewPager viewpager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

      int[]  viewpager_layouts = {
                R.layout.gender_layout,
              R.layout.age_layout,
              R.layout.profie_layout
        };
        next_btn = (Button)  findViewById(R.id.next_btn);


       viewpager = findViewById(R.id.viewpager);
        CircleIndicator indicator = findViewById(R.id.indicator);
        viewpager.setAdapter(new SamplePagerAdapter(viewpager_layouts));
        indicator.setViewPager(viewpager);


        viewpager.setOnTouchListener((v, event) -> true);
         next_btn.setOnClickListener(v -> viewpager.setCurrentItem(getItem(+1), true));
    }
    private int getItem(int i) {
        return viewpager.getCurrentItem() + i;
    }
}
