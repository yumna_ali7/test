package solutions.celeritas.com.sehat360.models.insertContent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class responseWrapper {
    @SerializedName("insertContentResponse")
    @Expose
    private InsertContentResponse insertContentResponse;

    public InsertContentResponse getInsertContentResponse() {
        return insertContentResponse;
    }

    public void setInsertContentResponse(InsertContentResponse insertContentResponse) {
        this.insertContentResponse = insertContentResponse;
    }
}
