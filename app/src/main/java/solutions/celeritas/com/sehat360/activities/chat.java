package solutions.celeritas.com.sehat360.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.amazonaws.HttpMethod;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.orm.SugarContext;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import solutions.celeritas.com.sehat360.R;
import solutions.celeritas.com.sehat360.adapters.ChatList_Adapter;
import solutions.celeritas.com.sehat360.models.Messages;
import solutions.celeritas.com.sehat360.networking.ApiController;
import solutions.celeritas.com.sehat360.utils.AudioRecordView;
import solutions.celeritas.com.sehat360.utils.Constants;
import solutions.celeritas.com.sehat360.utils.FileUtils;


public class chat extends AppCompatActivity implements AudioRecordView.RecordingListener {

    private long time;
    RecyclerView recyclerView;
    private AudioRecordView audioRecordView;
    EditText msg_edtxt;
    ImageView camera_btn;
    ImageView share_btn;
    ImageView send_btn;
    AmazonS3Client s3Client;
    File finalFile;
    URL presignedUrl;
    String msgText;
    String date_str;
    ArrayList<Messages> messagesArrayList;
    Messages messages;
    ChatList_Adapter chatList_adapter;

    public static int VIDEO_CAPTURED = 90;
    public static int DOCUMENT = 99;
    private Uri UrivideoFileUri;
    Uri doc_uri;
    String AudioSavePathInDevice = null;
    MediaRecorder mediaRecorder;
    Random random;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    private boolean isRecording;
    private int recordTime;
    private Handler handler;
    private int playTime;
    // private long mtime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        init();
        messagesArrayList.addAll(Messages.listAll(Messages.class));
    }


    public void init() {

        SugarContext.init(this);

        //   View view = getSupportActionBar().getCustomView();
        //   CustomTextViewBold title = view.findViewById(R.id.title);
        //  title.setText("Doctor's Visit");
        //Initializing views
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewMessages);
        audioRecordView = findViewById(R.id.recordingView);
        audioRecordView.setRecordingListener(this);
        // msg_edtxt = (EditText) findViewById(R.id.msg_edtxt);

        //  camera_btn = (ImageView) findViewById(R.id.Camera_btn);
        //  share_btn = (ImageView) findViewById(R.id.share_btn);
        //   send_btn = (ImageView) findViewById(R.id.send_btn);

        //getting data from arrayList
        messagesArrayList = new ArrayList<>();
        messages = new Messages();
        random = new Random();
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        chatList_adapter = new ChatList_Adapter(messagesArrayList, this);
        mLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(chatList_adapter);
        handler=new Handler();


        //requestPermission();
        //      setCamera_btn();
        //   setSend_btn();
        //    setShare_btn();
        setListener();
        isRecording=false;

    }

    private void setListener() {

        audioRecordView.getAttachmentView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showToast("Attachment");

                View view = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet, null);
                LinearLayout video_layout, document_layout, camera_layout;
                final BottomSheetDialog dialog = new BottomSheetDialog(chat.this);
                dialog.setContentView(view);
                video_layout = (LinearLayout) dialog.findViewById(R.id.video_layout);
                document_layout = (LinearLayout) dialog.findViewById(R.id.document_layout);
                video_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkPermission()) {
                            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            startActivityForResult(intent, VIDEO_CAPTURED);
                        }
                        dialog.dismiss();
                    }
                });
                document_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkPermission()) {
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setType("application/pdf");
                            startActivityForResult(Intent.createChooser(intent, "Select PDF"), DOCUMENT);
                        }
                        dialog.dismiss();
                    }
                });
                camera_layout = (LinearLayout) dialog.findViewById(R.id.camera_layout);
                camera_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        setCamera_btn();
                        dialog.dismiss();

                    }
                });

                dialog.show();


            }

        });


        //text message
        audioRecordView.getSendView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = audioRecordView.getMessageView().getText().toString();
                audioRecordView.getMessageView().setText("");
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
                date_str = sdf.format(c.getTime());


                if (!msg.isEmpty()) {
                    CapitalizeFirstLetter.capitaliseOnlyFirstLetter(msg);
                }

                audioRecordView.getMessageView().setInputType(
                        InputType.TYPE_CLASS_TEXT |
                                InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                );

                String url = String.valueOf(presignedUrl);

                if (msg.isEmpty()) {

                } else {
                    //  messagesArrayList.add(new Messages(msg,null,date_str,null,null,null));
                    messages = new Messages(msg, null, date_str, null, null, null, 0);
                    Log.e("text time", date_str);

                    recyclerView.scrollToPosition(chatList_adapter.getItemCount());
                    messagesArrayList.add(messages);
                    messages.save();


                    sendDataToServer("21", "6", Constants.Text_CONTENT_TYPE, "N/A", msgText);
                }

            }
        });
    }

    public void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);


        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try{
            mediaRecorder.start();
        }catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }

    public String CreateRandomAudioFileName(int string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        int i = 0;
        while (i < string) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));

            i++;
        }
        return stringBuilder.toString();
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void debug(String log) {
        Log.d("VarunJohn", log);
    }

    /**
     * Check and request permission
     *
     * @return
     */
    private boolean checkPermission() {

        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)

        ) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO},
                110);
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("permission", "Permission is granted");
                return true;
            } else {

                Log.v("permission", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 110);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("permission", "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 110:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //  Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    // main logic
                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                requestPermission();
                            }
                        }
                    }
                }
                break;
            default:
                break;

            /*    break;
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v("Permission: ", "Permission: " + permissions[0] + "was " + grantResults[0]);
                    //resume tasks needing this permission
                }
                break;*/

         /*   default:
                break;*/
            // }

        }
    }

    /**
     * Settings listener
     */

    public void setSend_btn() {
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   /* Calendar c = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
                    date_str = sdf.format(c.getTime());
                    msgText = msg_edtxt.getText().toString();
                    if (!msgText.isEmpty()) {
                        CapitalizeFirstLetter.capitaliseOnlyFirstLetter(msgText);
                    }

                    msg_edtxt.setInputType(
                            InputType.TYPE_CLASS_TEXT |
                                    InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                    );

                    String url = String.valueOf(presignedUrl);

                    if (msgText.isEmpty()) {

                    } else {

                        messages = new Messages(msgText, null, date_str, null, null);
                        Log.e("text time", date_str);

                        recyclerView.scrollToPosition(chatList_adapter.getItemCount());
                        messagesArrayList.add(messages);
                        messages.save();
                        msg_edtxt.setText("");

                        sendDataToServer("21", "6", Constants.Text_CONTENT_TYPE, "N/A", msgText);
                    }*/
            }
        });

    }


    public void setCamera_btn() {
        /*    camera_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {*/

        if (checkPermission()) {
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(chat.this);
        } else {
            requestPermission();
        }


        //   }
        //   });
    }

       /* private void setShare_btn () {
            share_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    View view = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet, null);
                    LinearLayout video_layout, document_layout;
                    BottomSheetDialog dialog = new BottomSheetDialog(chat.this);
                    dialog.setContentView(view);
                    video_layout = (LinearLayout) dialog.findViewById(R.id.video_layout);
                    document_layout = (LinearLayout) dialog.findViewById(R.id.document_layout);
                    video_layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (checkPermission()) {
                                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                startActivityForResult(intent, VIDEO_CAPTURED);
                            }

                        }
                    });
                    document_layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (checkPermission()) {
                                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                                intent.setType("application/pdf");
                                startActivityForResult(Intent.createChooser(intent, "Select PDF"), DOCUMENT);
                            }
                        }
                    });

                    dialog.show();


                }
            });

        }
*/


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            //Capture Image
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();
                Log.e("uri", "" + resultUri);
                finalFile = new File(resultUri.getPath());
                String fileName = finalFile.getName().replace("cropped", "Image");
                Log.e("filename", finalFile.getName().replace("cropped", "Image"));

                setImages(resultUri.toString());
                uploadPhotoToS3(finalFile, fileName);
                sendDataToServer("21", "6", Constants.IMG_CONTENT_TYPE, fileName, "N/A");


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

        //For Capture Video
        if (requestCode == VIDEO_CAPTURED && resultCode == RESULT_OK) {
            UrivideoFileUri = data.getData();

            String path = getRealPathFromURI(UrivideoFileUri);
            finalFile = new File(path);
            Log.e("videoname", "" + finalFile.getPath());
            String filename = finalFile.getName();

            setVideoCaptured(UrivideoFileUri.toString());
            uploadPhotoToS3(finalFile, filename);
            sendDataToServer("21", "6", Constants.Video_CONTENT_TYPE, filename, "N/A");
        }


        //for Document
        if (requestCode == DOCUMENT && resultCode == RESULT_OK) {
            doc_uri = data.getData();

            if (doc_uri != null) {
                finalFile = new File(FileUtils.getPath(this, doc_uri));
            }
            Log.e("path", "" + finalFile);
            if (finalFile.exists()){
                setDocuments(finalFile.getPath());
            uploadPhotoToS3(finalFile, finalFile.getName());
            sendDataToServer("21", "6", Constants.PDF_CONTENT_TYPE, finalFile.getName(), "N/A");
        }
            else {
                Toast.makeText(this, "The file not exists!", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};

        CursorLoader cursorLoader = new CursorLoader(
                this,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        // Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void setDocuments(String uri) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        date_str = sdf.format(c.getTime());

        messages = new Messages(null, null, date_str, null, uri, null, 0);
        Log.e("doc", uri);

        recyclerView.scrollToPosition(chatList_adapter.getItemCount());
        messagesArrayList.add(messages);
        messages.save();
        chatList_adapter.notifyDataSetChanged();

    }

    public void setVideoCaptured(String uri) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        date_str = sdf.format(c.getTime());

        messages = new Messages(null, null, date_str, uri, null, null, 0);
        recyclerView.scrollToPosition(chatList_adapter.getItemCount());
        messagesArrayList.add(messages);
        messages.save();
        chatList_adapter.notifyDataSetChanged();
    }


    public void setImages(String uri) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        date_str = sdf.format(c.getTime());

        messages = new Messages(null, uri, date_str, null, null, null, 0);

        Log.e("img time", date_str);
        recyclerView.scrollToPosition(chatList_adapter.getItemCount());
        messagesArrayList.add(messages);
        messages.save();
        chatList_adapter.notifyDataSetChanged();
    }


    //upload Image to S3 Bucket

    private void uploadPhotoToS3(final File path, final String fileName) {
        AWSMobileClient.getInstance().initialize(this).execute();
        // KEY and SECRET are gotten when we create an IAM user above
        BasicAWSCredentials credentials = new BasicAWSCredentials(Constants.KEY, Constants.SECRET);
        s3Client = new AmazonS3Client(credentials);

        TransferUtility transferUtility =
                TransferUtility.builder()
                        .context(getApplicationContext())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(s3Client)
                        .build();

        TransferObserver uploadObserver =
                transferUtility.upload(Constants.BUCKET_NAME, fileName, path);


        uploadObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    Log.e("state", "" + state);
                    // Handle a completed download.
                    //  Toast.makeText(chat.this, "Upload Completed!", Toast.LENGTH_SHORT).show();
                    getImageUrl(fileName);


                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            }

            @Override
            public void onError(int id, Exception ex) {
                //Toast.makeText(chat.this, "error!" + ex, Toast.LENGTH_SHORT).show();
                Log.e("", "" + ex);
            }
        });


// If your upload does not trigger the onStateChanged method inside your
// TransferListener, you can directly check the transfer state as shown here.
        if (TransferState.COMPLETED == uploadObserver.getState()) {
            // Handle a completed upload.
        }
    }


    public void getImageUrl(String ImgKey) {

        GeneratePresignedUrlRequest generatePresignedUrlRequest =
                new GeneratePresignedUrlRequest(Constants.BUCKET_NAME, ImgKey)
                        .withMethod(HttpMethod.GET);
        presignedUrl = s3Client.generatePresignedUrl(generatePresignedUrlRequest);

        System.out.println("Pre-Signed URL =" + presignedUrl);
    }

    public void sendDataToServer(String user_id, String section_id, String content_type, String content_location, String text) {
        ApiController.getInstance(this).getAPIService().InsertContent(user_id, section_id, content_type, content_location, text).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                if (response.isSuccessful()) {
                    //   Toast.makeText(chat.this, "Content added successfully", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                Log.e("TAG", "response 33: " + t);
            }
        });
    }


/*    public void createFolder() {
     AudioSavePathInDevice =CreateRandomAudioFileName(5) + "AudioRecording.3gp";
        FileOutputStream out = null;
        try {
            File path=new File(getFilesDir()+"/"+ AudioSavePathInDevice);
            Log.e("File",""+path);
           // File mypath=new File(path,AudioSavePathInDevice);
            if (!path.exists()) {
                try {
                    out = new FileOutputStream( path );
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                if (out != null) {
                    out.write(AudioSavePathInDevice.getBytes());
                }
                out.close();
            }
        }catch (Exception e){

        }




        *//**
         * Creating file package but showing Read only exception
         *//*
    *//*    FileOutputStream outputStream = null;
        AudioSavePathInDevice =CreateRandomAudioFileName(5)+"AudioRecording.3gp";
        try {
          outputStream = new FileOutputStream(  new File(getFilesDir()+"/"+ AudioSavePathInDevice));
          Log.e("filedirectory",""+new File(getFilesDir()+"/"+ AudioSavePathInDevice));
          //  outputStream = openFileOutput(AudioSavePathInDevice, Context.MODE_PRIVATE);
            outputStream.write(AudioSavePathInDevice.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*//*


        */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopRecording();
    }


    /**
         * Creating file in folder recordings
         *//*

     *//*  File mediaStorageDir = new File ( Environment.getExternalStorageDirectory().getAbsoluteFile() ,"Recordings");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("App", "failed to create directory");
            }
        }
        AudioSavePathInDevice = mediaStorageDir + "/"+ CreateRandomAudioFileName(5)+"AudioRecording.3gp";*//*
    }*/

    public void stopRecording(){
        try {
            mediaRecorder.stop();
            mediaRecorder.release();
            mediaRecorder = null;
        }
        catch (RuntimeException ex){
            debug("MediaRecorder stop failed");
        }

    }

    @Override
    public void onRecordingStarted() {
        debug("started");
        if (checkPermission()) {

            AudioSavePathInDevice =
                    Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                            CreateRandomAudioFileName(5) + "AudioRecording.mp3";

          //  startRecording();
            MediaRecorderReady();

           /* try {
                mediaRecorder.prepare();
                mediaRecorder.start();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }*/
        /*    Toast.makeText(chat.this, "Recording started",
                    Toast.LENGTH_LONG).show();*/
        } else {
            requestPermission();
        }

        time = System.currentTimeMillis() / (1000);
    }

    @Override
    public void onRecordingLocked() {
        debug("locked");
    }

    @Override
    public void onRecordingCompleted() {
        debug("completed");
        if (mediaRecorder != null){
            try{
                mediaRecorder.stop();
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        date_str = sdf.format(c.getTime());

        int recordTime = (int) ((System.currentTimeMillis() / (1000)) - time);
        if (recordTime > 1) {
            messages = new Messages(recordTime, AudioSavePathInDevice, date_str);
            recyclerView.scrollToPosition(chatList_adapter.getItemCount());
            messagesArrayList.add(messages);
            messages.save();
            chatList_adapter.notifyDataSetChanged();

            File file = new File(AudioSavePathInDevice);
            Log.e("file", "" + file.getName());
            String Filename = file.getName();
            sendDataToServer("21", "6", Constants.Audio_CONTENT_TYPE, Filename, "N/A");
         //   uploadPhotoToS3(file,Filename);

        }

    }

    @Override
    public void onRecordingCanceled() {
        debug("canceled");
        stopRecording();
    }


}

class CapitalizeFirstLetter {
    public static String capitaliseName(String name) {
        String collect[] = name.split(" ");
        String returnName = "";
        for (int i = 0; i < collect.length; i++) {
            collect[i] = collect[i].trim().toLowerCase();
            if (collect[i].isEmpty() == false) {
                returnName = returnName + collect[i].substring(0, 1).toUpperCase() + collect[i].substring(1) + " ";
            }
        }
        return returnName.trim();
    }

    public static String capitaliseOnlyFirstLetter(String data) {
        return data.substring(0, 1).toUpperCase() + data.substring(1);
    }
}





