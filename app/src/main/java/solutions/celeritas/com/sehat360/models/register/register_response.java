package solutions.celeritas.com.sehat360.models.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class register_response {

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("result")
        @Expose
        private List<Object> result = null;

        public Boolean getSuccess() {
            return success;
        }

        public Boolean setSuccess(Boolean success) {
            this.success = success;
            return success;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Object> getResult() {
            return result;
        }

        public void setResult(List<Object> result) {
            this.result = result;
        }


}
